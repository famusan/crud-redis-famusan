package net.javaguides.springboot.controller;

import java.util.List;

import net.javaguides.springboot.model.MasterPriceRedis;
import net.javaguides.springboot.repository.MasterPriceRedisRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import net.javaguides.springboot.model.MasterPrice;
import net.javaguides.springboot.service.MasterPriceService;

@Controller
public class MasterPriceController {
	@Autowired
	private MasterPriceService masterPriceService;

	@Autowired
	private MasterPriceRedisRepo masterPriceRedisRepo;

	@GetMapping("/")
	public String viewHomePage(Model model) {
		return findPaginated(1, "originCd", "asc", model);
	}

	@GetMapping("/showNewMasterPriceForm")
	public String showNewMasterPriceForm(Model model) {
		MasterPrice masterPrice = new MasterPrice();
		model.addAttribute("masterPrice", masterPrice);
		return "new_masterPrice";
	}

	@PostMapping("/saveMasterPrice")
	public String saveMasterPrice(@ModelAttribute("masterPrice") MasterPrice masterPrice) {
		masterPriceService.saveMasterPrice(masterPrice);
		MasterPriceRedis masterPriceRedis = new MasterPriceRedis();
		masterPriceRedis.setId(masterPrice.getId().intValue());
		masterPriceRedis.setOriginCd(masterPrice.getOriginCd());
		masterPriceRedis.setDestinationCd(masterPrice.getDestinationCd());
		masterPriceRedis.setProduct(masterPrice.getProduct());
		masterPriceRedis.setPrice(masterPrice.getPrice().intValue());
		masterPriceRedisRepo.save(masterPriceRedis);

		return "redirect:/";
	}

	@GetMapping("/showFormForUpdate/{id}")
	public String showFormForUpdate(@PathVariable(value = "id") long id, Model model) {

		MasterPrice masterPrice = masterPriceService.getMasterPriceById(id);

		model.addAttribute("masterPrice", masterPrice);
		return "update_masterPrice";
	}

	@GetMapping("/deleteMasterPrice/{id}")
	public String deleteMasterPrice(@PathVariable(value = "id") Long id) {

		this.masterPriceService.deleteMasterPriceById(id);
		return "redirect:/";
	}

	@GetMapping("/page/{pageNo}")
	public String findPaginated(@PathVariable(value = "pageNo") int pageNo, @RequestParam("sortField") String sortField,
			@RequestParam("sortDir") String sortDir, Model model) {
		int pageSize = 5;

		Page<MasterPrice> page = masterPriceService.findPaginated(pageNo, pageSize, sortField, sortDir);
		List<MasterPrice> listMasterPrice = page.getContent();

		model.addAttribute("currentPage", pageNo);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());

		model.addAttribute("sortField", sortField);
		model.addAttribute("sortDir", sortDir);
		model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");

		model.addAttribute("listMasterPrice", listMasterPrice);
		return "index";
	}
}

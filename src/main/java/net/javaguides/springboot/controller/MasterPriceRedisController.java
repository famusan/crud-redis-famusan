package net.javaguides.springboot.controller;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.javaguides.springboot.model.MasterPriceRedis;
import net.javaguides.springboot.repository.MasterPriceRedisRepo;

@RestController
@RequestMapping("/masterpriceredis")
public class MasterPriceRedisController {
	@Autowired
	private MasterPriceRedisRepo masterPriceRedisRepo;
	
	@PostMapping
	public MasterPriceRedis save(@RequestBody MasterPriceRedis masterPriceRedis) {
		return masterPriceRedisRepo.save(masterPriceRedis);
	}
	
	@GetMapping
	public List<MasterPriceRedis> getAllMasterPriceRedis(){
		return masterPriceRedisRepo.findAll();
	}

	@GetMapping("/{id}")
	public Object findMasterPriceRedis(@PathVariable int id){
		return masterPriceRedisRepo.findByMasterPriceRedisId(id);
	}

	
	@DeleteMapping("/{id}")
	public String remove(@PathVariable int id){
		return masterPriceRedisRepo.deleteMasterPriceRedis(id);
	}

}

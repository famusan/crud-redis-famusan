package net.javaguides.springboot.repository;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import net.javaguides.springboot.model.MasterPriceRedis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class MasterPriceRedisRepo {
	public static final String HASH_KEY = "PRICE";
	@Autowired
	private RedisTemplate template;

	public MasterPriceRedis save(MasterPriceRedis masterPriceRedis) {
		template.opsForHash().put(HASH_KEY, masterPriceRedis.getId(), masterPriceRedis);
		return masterPriceRedis;
	}

	public List<MasterPriceRedis> findAll() {
		return template.opsForHash().values(HASH_KEY);
	}

	public Object findByMasterPriceRedisId(int id) {
		Object o = (Object) template.opsForHash().get(HASH_KEY, id);
		return o;
	}

	public Object findByMasterPriceRedisOriginCd(String originCd) {
		return template.opsForHash().get(HASH_KEY, originCd);
	}

	public String deleteMasterPriceRedis(int id) {
		template.opsForHash().delete(HASH_KEY, id);
		return "Master Price Redis removed";
	}
}

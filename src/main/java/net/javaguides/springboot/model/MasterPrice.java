package net.javaguides.springboot.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "master_price")
public class MasterPrice {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "origin_code")
	private String originCd;

	@Column(name = "destination_code")
	private String destinationCd;

	@Column(name = "price")
	private Double price;

	@Column(name = "product")
	private String product;

	public MasterPrice() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOriginCd() {
		return originCd;
	}

	public void setOriginCd(String originCd) {
		this.originCd = originCd;
	}

	public String getDestinationCd() {
		return destinationCd;
	}

	public void setDestinationCd(String destinationCd) {
		this.destinationCd = destinationCd;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	@Override
	public String toString() {
		return "MasterPrice [id=" + id + ", originCd=" + originCd + ", destinationCd=" + destinationCd + ", price="
				+ price + ", product=" + product + "]";
	}

}

package net.javaguides.springboot.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import net.javaguides.springboot.model.MasterPrice;
import net.javaguides.springboot.repository.MasterPriceRepo;

@Service
public class MasterPriceServiceImpl implements MasterPriceService {
	@Autowired
	private MasterPriceRepo masterPriceRepo;
	
	@Override
	public List<MasterPrice> getAllMasterPrices() {
		return masterPriceRepo.findAll();
	}

	@Override
	public void saveMasterPrice(MasterPrice masterPrice) {
		this.masterPriceRepo.save(masterPrice);
	}

	@Override
	public MasterPrice getMasterPriceById(long id) {
		Optional<MasterPrice> optional = masterPriceRepo.findById(id);
		MasterPrice masterPrice = null;
		if (optional.isPresent()) {
			masterPrice = optional.get();
		} else {
			throw new RuntimeException(" Employee not found for id :: " + id);
		}
		return masterPrice;
	}

	@Override
	public void deleteMasterPriceById(long id) {
		// TODO Auto-generated method stub
		this.masterPriceRepo.deleteById(id);
		
	}

	@Override
	public Page<MasterPrice> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection) {
		Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() :
			Sort.by(sortField).descending();
		
		Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);
		return this.masterPriceRepo.findAll(pageable);
	}

}

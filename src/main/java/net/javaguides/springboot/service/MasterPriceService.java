package net.javaguides.springboot.service;

import java.util.List;

import org.springframework.data.domain.Page;
import net.javaguides.springboot.model.MasterPrice;

public interface MasterPriceService {
	List<MasterPrice> getAllMasterPrices();

	void saveMasterPrice(MasterPrice masterPrice);

	MasterPrice getMasterPriceById(long id);

	void deleteMasterPriceById(long id);

	Page<MasterPrice> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection);

}
